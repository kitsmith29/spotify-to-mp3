from django.shortcuts import render
from rest_framework import viewsets
from .serializers import SpotifyUserSerializer
from .models import SpotifyUser

class SpotifyUserView(viewsets.ModelViewSet):
    serializer_class = SpotifyUserSerializer
    queryset = SpotifyUser.objects.all()