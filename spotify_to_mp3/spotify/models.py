from django.db import models


class SpotifyUser(models.Model):
    user = models.CharField(max_length=95)

    def __str__(self):
        return self.user
